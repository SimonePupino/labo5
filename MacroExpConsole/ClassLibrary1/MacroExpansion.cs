﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary1
{
    public class MacroExpansion<T> : IEnumerable<T>
    {
        private static T[] _result;
        private static int _top;
        public static T[] MacroExp(T[] sequence, T value, T[] newValue)
        {
            if (sequence == null || newValue == null) throw new ArgumentNullException();
            var numberOfValues = sequence.Count(element => element.Equals(value));
            var lenght = sequence.Length + (newValue.Length * numberOfValues) - numberOfValues;
            _result = new T[lenght];
            foreach (var element in sequence)
            { 
                if (element.Equals(value)) foreach (var substitute in newValue) Push(substitute);        
                else Push(element);
            }
            return _result;
        }

        private static void Push(T t)
        {
            _result[_top] = t;
            _top++;
        }


        public IEnumerator<T> GetEnumerator()
        {
            for (var index = 0; index < _top; index++) yield return _result[index];

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}