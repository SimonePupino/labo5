﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ClassLibrary1;

namespace ClassLibrary1.Test
{
    [TestFixture]
    public class MacroExpansionTest
    {
        [Test]
        public void ArgumentExceptionNewValue() => Assert.Throws<ArgumentNullException>(delegate { MacroExpansion<int>.MacroExp(new[] {1, 2, 1, 2, 3}, 2, null);});
        [Test]
        public void ArgumentExceptionSequence() => Assert.Throws<ArgumentNullException>(delegate { MacroExpansion<int>.MacroExp(null, 2, new []{7, 8, 9}); });
        [Test]
        public void ArgumentExceptionAll() => Assert.Throws<ArgumentNullException>(delegate { MacroExpansion<int>.MacroExp(null, 2, null); });
        [Test]
        public void InstantiateOne() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] {1, 2}, 2, new[] {7, 8, 9}),
                Is.EqualTo(new[] {1, 7, 8, 9}));
        [Test]
        public void InstantiateTwo() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] { 1, 2, 2 }, 2, new[] { 7, 8, 9 }),
                Is.EqualTo(new[] { 1, 7, 8, 9, 7, 8, 9 }));
        [Test]
        public void InstantiateThree() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] { 1, 2, 1, 2 }, 2, new[] { 7, 8, 9 }),
                Is.EqualTo(new[] { 1, 7, 8, 9, 1, 7, 8, 9 }));
        [Test]
        public void InstantiateFour() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] { 2, 1, 2 }, 2, new[] { 7, 8, 9 }),
                Is.EqualTo(new[] { 7, 8, 9, 1, 7, 8, 9 }));
        [Test]
        public void InstantiateFive() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] { 1, 2, 1, 2 }, 3, new[] { 7, 8, 9 }),
                Is.EqualTo(new[] { 1, 2, 1, 2 }));
        [Test]
        public void InstantiateSix() =>
            Assert.That(MacroExpansion<int>.MacroExp(new[] { 1 }, 1, new[] { 7, 8, 9 }),
                Is.EqualTo(new[] { 7, 8, 9}));
    }
}
